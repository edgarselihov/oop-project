import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;

public class returnUnique {

    static ArrayList <Integer> returnUniqueList (ArrayList<Integer> list, ArrayList <BufferedImage> fakeImages) {
        // to store unique values
        ArrayList <Integer> result = new ArrayList<>();
        // Record encountered Integers in HashSet
        HashSet <Integer> set = new HashSet<>();
        // iterate through list "list"
        for (Integer item: list) {
            // if set doesnt have any elements then add it to "result" and "set" arrayLists
            if(!set.contains(item)) {
                result.add(item);
                set.add(item);
            }
        }
        // as "result" length will be shorter than "list", we need to add one more random element
        if (result.size() != list.size()) {
            int temp = returnRandom.returnRandomNumber(fakeImages.size());
            result.add(temp);
            // some dull solution for checking whether second and third elements are the same
            if(result.get(1).equals(result.get(2))) {
                result.remove(result.get(2));
                temp = returnRandom.returnRandomNumber(fakeImages.size() - 1);
                result.add(temp);
            }
        }
        return result;
    }
}