import java.util.ArrayList;
import java.util.Collections;

public class saveUsername implements Comparable <saveUsername> {

    private String username;
    private int score;

    saveUsername(String username, int score) {
        this.username = username;
        this.score = score;
    }

    public String getUsername() {
        return this.username;
    }

    public int getScore() {
        return this.score;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return this.username + ": " + this.score;
    }

    public static void getTop5(ArrayList <saveUsername> usersPlayed) {
        // if less than 5 players played
        if (usersPlayed.size() < 5) {
            // sort in descending order
            Collections.sort(usersPlayed);
            System.out.println("Best players so far: ");
            for(saveUsername elem: usersPlayed) {
                System.out.println(elem);
            }
        }
        // if more than 5 then show only top five
        if (usersPlayed.size() >= 5) {
            // sort by points in descending order
            Collections.sort(usersPlayed);
            System.out.println("Top 5 players: ");
            for(int i = 0; i < 5; i++) {
                System.out.println(usersPlayed.get(i));
            }
        }
        // obvious
        if (usersPlayed.size() == 0) {
            System.out.println("No players played yet");
        }
    }

    @Override
    // method for comparing elements
    public int compareTo(saveUsername comparePlayers) {
        int comparePoints = ((saveUsername)comparePlayers).getScore();
        return comparePoints - this.score;
    }
}