import java.awt.*;
import java.util.*;
import java.awt.image.BufferedImage;
import javax.swing.*;

public class Main {

    private Main() {

        JFrame frame;
        JPanel panel1, panel2, panel3, panel4, panel5;
        JLabel label1, label2, label3, label4;

        JPanel[] panels = new JPanel[4]; //massiv labelite jaoks

        getPictures fake = new getPictures("fake");
        getPictures real = new getPictures("real");

        Scanner scanner;
        Scanner sc;
        // init arraylist for images
        ArrayList <BufferedImage> fakeImages;
        ArrayList <BufferedImage> realImages;
        ArrayList <saveUsername> usersPlayed = new ArrayList<>();

        int isCorrect, picInput, action, points;

        String userName, agreeToChange, continueInput, changeName;
        saveUsername userData;
        // create two objects of pictures and return arraylist of images
        fakeImages = fake.returnPics(fake.getPathname());
        realImages = real.returnPics(real.getPathname());

        scanner = new Scanner(System.in);

        printChoices();
        // make infinite loop unless quit choice is pressed
        while (true) {

            points = 15;
            action = scanner.nextInt();

            if (action == 1) {
                // receive input from user
                sc = new Scanner(System.in);
                System.out.print("Enter user name: ");
                userName = sc.nextLine();
                System.out.println("Welcome to game, " + userName + "\nYour points: " + points);

                do {

                    ArrayList<Integer> intElements = new ArrayList<>();
                    ArrayList<Integer> uniqueElements;
                    // adding random numbers to arraylist intElements
                    for (int i = 0; i < 3; i++) {
                        intElements.add(returnRandom.returnRandomNumber(fakeImages.size() - 1));
                    }

                    System.out.println(intElements);
                    uniqueElements = returnUnique.returnUniqueList(intElements, fakeImages);
                    System.out.println(uniqueElements);
                    // create an ew object of Jlabel
                    // fakeImages arraylist element gets first element of uniqueElement list
                    // then resize image
                    label1 = new JLabel(new ImageIcon((fakeImages.get(uniqueElements.get(0))
                            .getScaledInstance(250, 250, Image.SCALE_FAST))));

                    label2 = new JLabel(new ImageIcon((fakeImages.get(uniqueElements.get(1))
                            .getScaledInstance(250, 250, Image.SCALE_FAST))));

                    label3 = new JLabel(new ImageIcon((fakeImages.get(uniqueElements.get(2))
                            .getScaledInstance(250, 250, Image.SCALE_FAST))));

                    label4 = new JLabel(new ImageIcon((realImages
                            .get(returnRandom.returnRandomNumber(realImages.size() - 1))
                            .getScaledInstance(250, 250, Image.SCALE_FAST))));

                    //make frame and panels
                    frame = new JFrame("App");
                    frame.setSize(800, 480);

                    //määran ära, mitu pilti korraga
                    panel1 = new JPanel();
                    panel2 = new JPanel();
                    panel3 = new JPanel();
                    panel4 = new JPanel();
                    panel5 = new JPanel(new GridLayout(2, 2));

                    // add images to panel
                    panel1.add(label1);
                    panel2.add(label2);
                    panel3.add(label3);
                    panel4.add(label4);

                    panels[0] = panel1;
                    panels[1] = panel2;
                    panels[2] = panel3;
                    panels[3] = panel4;

                    //shuffeling images positions
                    Collections.shuffle(Arrays.asList(panels));
                    // to know where correct image will be after shuffle
                    isCorrect = Arrays.asList(panels).indexOf(panel4);
                    // and add them to main panel
                    for (JPanel panel : panels) {

                        panel5.add(panel);
                    }

                    frame.add(panel5);
                    frame.setVisible(true);
                    frame.pack();
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

                    System.out.print("Which pic is of real person? Press 1, 2, 3 or 4: ");

                    picInput = scanner.nextInt();

                    if (picInput - 1 == isCorrect) {

                        points += 7;
                        System.out.println("Correct!");

                    } else {

                        points -= 8;
                        System.out.println("Wrong!");

                        if (points <= 0) {
                            System.out.println("Oops! You ran out of points!");
                            System.out.println("Game over");
                            frame.dispose();
                            break;
                        }
                    }

                    System.out.println("Your current points = " + points);
                    System.out.print("Again? Press Y or N ");
                    continueInput = scanner.next();
                    // if user wants to continue pictures are updated
                    // otherwise offers to change nick name
                    // and afterwards show main menu
                    if (continueInput.equalsIgnoreCase("Yes")
                            || continueInput.equalsIgnoreCase("Y")) {
                        frame.dispose();
                        continue;
                    }

                    if (continueInput.equalsIgnoreCase("No")
                            || continueInput.equalsIgnoreCase("N")) {
                        frame.dispose();
                        break;

                    }

                } while (points > 0);
                // create an object to pass game results
                // and if user wants to change user's name and
                userData = new saveUsername(userName, points);
                System.out.println("Your final score: " + points);

                System.out.println("If you wish to change your nick name hit 'Y' ");
                // here is condition if user wants to update nickname
                if ((agreeToChange = sc.nextLine()).equalsIgnoreCase("y")) {
                    System.out.print("Enter a new name: ");
                    changeName = sc.nextLine();
                    System.out.println("Username changed from " + userName + " to " + changeName);
                    userData.setUsername(changeName);
                }
                // add player's result to usersPlayed arraylist
                usersPlayed.add(userData);
                printChoices();

            }

            if (action == 2) {
                // shows top 5 players (remark: it doesnt save players if game was force-restarted)
                saveUsername.getTop5(usersPlayed);
                printChoices();
            }

            if (action == 3) {
                System.out.println("Exiting program");
                System.exit(0);
            }
        }
    }

    private static void printChoices() {
        System.out.println("Press corresponding number to choose the action: \n");
        System.out.println("1. Start a new game\n2. Show best top 5 players\n3. Quit");
    }

    public static void main(String[] args) {
        new Main();
    }
}