
import java.io.IOException;
import java.util.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

public class getPictures {
    private String pathname;

    getPictures(String pathname) {
        this.pathname = pathname;
    }

    public String getPathname() {
        return pathname;
    }

    public ArrayList <BufferedImage> returnPics(String pathname) {
        BufferedImage img;
        File dir_folder = new File(pathname);
        ArrayList <BufferedImage> images = new ArrayList<>();
        // add images for dir location into images arraylist
        for (final File f : dir_folder.listFiles()) {

            try {

                img = ImageIO.read(f);
                images.add(img);
            }

            catch (final IOException e) {
                System.out.println("Pictures not found");
            }
        }

        return images;
    }
}
